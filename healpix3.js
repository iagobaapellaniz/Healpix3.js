/*  Copyright (c) 2016 Iagoba Apellaniz

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// This stands private
function normalizeLatLng (lat, lng) {
  lng = lng / Math.PI * 2 - 4 * Math.round(lng/4)
  if (lng < 0) { lng = lng + 4 }
  lat = Math.sin(lat)*0.75
  return [lng, lat]
}

function convertToXY (lat, lng) {
  let v_ang = normalizeLatLng(lat, lng)
  let v = [0.,0.]
  if (v_ang[1] > 0.5 || v_ang[1] < -0.5) {
    if (Math.sign(v_ang[1]) == 1) {
      v_ang[1] = 1 - Math.sqrt(0.75 - v_ang[1])
    } else {
      v_ang[1] = - 1 + Math.sqrt(0.75 + v_ang[1])
    }
    if (0<=v_ang[0] && v_ang[0]<1) {
      v_ang[0] = (v_ang[0] - 0.5) * (1 - Math.abs(v_ang[1])) * 2 + 0.5
    } else if (1<=v_ang[0] && v_ang[0]<2) {
      v_ang[0] = (v_ang[0] - 1.5) * (1 - Math.abs(v_ang[1])) * 2 + 1.5
    } else if (2<=v_ang[0] && v_ang[0]<3) {
      v_ang[0] = (v_ang[0] - 2.5) * (1 - Math.abs(v_ang[1])) * 2 + 2.5
    } else {
      v_ang[0] = (v_ang[0] - 3.5) * (1 - Math.abs(v_ang[1])) * 2 + 3.5
    }
  }
  v[0] = v_ang[0] - v_ang[1] + 0.5
  v[1] = v_ang[0] + v_ang[1] - 0.5
  if (v[1]<0) {
    v[0] = v[0] + 4
    v[1] = v[1] + 4
  }
  return v
}

function cellNumber(subdiv, lat, lng) {
  // Return a cell number
  let cbs = Math.pow(2,subdiv)
  let v = convertToXY(lat, lng)
  let fl = [Math.floor(v[0]*cbs), Math.floor(v[1]*cbs)]
  for (let i = 1; i<=4; i++) {
    if (fl[1] == i*cbs && fl[0] < i*cbs) { fl[1] = fl[1]-1}
    if ((i-1)*cbs <= fl[1] && fl[1] < i*cbs) {
      fl[0] = fl[0] - (i-1)*cbs
    }
  }
  if (fl[1] == 5) { fl[1] = fl[1]-5 }
  if (fl[0] == 3*cbs) { fl[0] = fl[0]-1 }
  return fl[1]*3*cbs + fl[0]
}

function cellNeighbors(subdiv, cell) {
  let cbs = Math.pow(2,subdiv)
  let cpr = 3*cbs
  let row = Math.floor(cell/cpr)
  let col = cell - row * cpr
  let relrow = row % cbs

  console.log("Cells per base:" + cbs);
  console.log("Cells per row: " + cpr);
  console.log("Current ROW:   " + row);
  console.log("Relative ROW:  " + relrow);
  console.log("Current Column:" + col);

  let result = [];

  if (relrow == 0) {
    if (col == 0) {
      result[0] = cell + cpr
      result[1] = cell - 2*cbs - 1
      result[2] = cell + 1
      result[3] = cell - 2*cbs
    } else if (col == cpr-1) {
      result[0] = cell + cpr
      result[1] = cell - 1
      result[2] = 1
    } else if (col >= 2*cbs) {

    } else {
      result[0] = cell + cpr
      result[1] = cell - 1
      result[2] = cell + 1
      result[3] = cell - cpr
    }
  } else if (relrow == cbs-1) {

  } else {
      result[0] = cell + cpr
      result[3] = cell - cpr
    if (col == 0) {
      result[0] = (row - relrow + 1) * cpr + cbs - relrow -1
      result[2] = cell + 1
    } else if (col == cpr-1) {
      result[1] = cell - 1
      result[2] = (row - relrow + cbs) * cpr - relrow - 1
    } else {
      result[1] = cell - 1
      result[2] = cell + 1
    }
  }

  for (let i=0; i<4; i++) {
    if (result[i] >= 12*cbs) {result[i] = result[i] - 12*cbs}
    if (result[i] < 0) {result[i] = result[i] + 12*cbs}
  }

  return result
}

function ringCellNumber(subdiv, cell) {
  let cbs = Math.pow(2,subdiv)
  let cpr = 3*cbs
  let row = Math.floor(cell/cpr)
  let relcol = cell - row * cpr
  let relrow = row % cbs
  let bigrow = (row - relrow) / cbs
  let col = cbs * bigrow + relcol

  if (relrow - relcol - 1 >= 0) {
    console.log("top");
    return 4 * (cbs - (relrow - relcol) - 1) * (cbs - (relrow - relcol)) / 2 + bigrow * (cbs - (relrow - relcol)) + relcol
  } else if (relrow - relcol + 2 * cbs + 1<= 0) {
    console.log("bottom");
    return 4 * cpr * cbs - 4 * (cpr - (relcol - relrow)) * (cpr - (relcol - relrow) + 1) / 2 + bigrow * (cpr - (relcol - relrow)) + relrow
  } else {
    console.log("middle");
    return 4 * (cbs - 1) * cbs / 2 + (col - row) * 4 * cbs + row
  }
}

function nestCellNumber(order, cell) {
  let cbs = Math.pow(2,order-1)
}

function nestToCell(order, ncell) {
  let cbs = Math.pow(2,order-1)
}

function ringToCell(order, rcell) {
  let cbs = Math.pow(2,order-1)
}

function meanAngleFromCell(order, cell) {
  let cbs = Math.pow(2,order-1)
}

exports.convertToXY = convertToXY;
exports.cellNumber = cellNumber;
exports.cellNeighbors = cellNeighbors;
exports.ringCellNumber = ringCellNumber;
exports.nestCellNumber = nestCellNumber;
exports.ringToCell = ringToCell;
exports.nestToCell = nestToCell;
exports.meanAngleFromCell = meanAngleFromCell;
