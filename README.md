# Simple implementation of the HEALPix algorithm

This package provides some functionality for mapping solid angles of a sphere into a 2D plane.

## Installing and call

**NOT WORKING YET** As all packages of **Node.js** this is installed using
```
$ npm install -save healpix3
```
and loaded by the command
```
var hp = require('healpix3')
```

## Methods provided with this package

- **Name:** `convertToXY(longitude, latitude)`
  - **Return:** Array of x-coordinate and y-coordinate.
  - **Arguments:** Longitude and latitude in radians. The longitude can be any real number while the latitude must be in between `-Math.PI/2` and `Math.PI/2` both included.
  - **Code snipet:**
  ```javascript
  > hp.convertToXY(longitude, latitude)
  [x, y]
  ```
  - **Description:** This methods takes any solid angle, $\phi$ for the longitude angle and $\theta$ for the latitude angle, and converts it to a pair of values representing the solid angle into a coordinate system where the area is scaled by $1/\pi r^2$ no-matter which solid angle is passed to the method. These coordinates follow somehow the HEALPix algorithm.
- **Name:**`cellNumber(subdiv, longitude, latitude)`
  - **Return:** Integer for the cell number
  - **Arguments:**
  - **Code snipet:**
  ```javascript
  > hp.cellNumber(subdiv, longitude, latitude)
  ncell
  ```
  - **Description:**
- **Name:**`neighborCells(subdiv, cellNumber)`
  - **Return:**
  - **Arguments:**
  - **Code snipet:**
  - **Description:**
- **Name:**`ringCellNumber(subdiv, cellNumber)`
  - **Return:**
  - **Arguments:**
  - **Code snipet:**
  - **Description:**
- **Name:**`nestCellNumber(subdiv, cellNumber)`
  - **Return:**
  - **Arguments:**
  - **Code snipet:**
  - **Description:**
- **Name:**`ringToCell(subdiv, ringNumber)`
  - **Return:**
  - **Arguments:**
  - **Code snipet:**
  - **Description:**
- **Name:**`nestToCell(subdiv, nestNumber)`
  - **Return:**
  - **Arguments:**
  - **Code snipet:**
  - **Description:**
